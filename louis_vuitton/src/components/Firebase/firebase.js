import app from 'firebase/app';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyDqKZByN1l5MaAIssG9-84e9zzcedP53Vc",
    authDomain: "projet-louis-vuitton-promo2.firebaseapp.com",
    databaseURL: "https://projet-louis-vuitton-promo2.firebaseio.com",
    projectId: "projet-louis-vuitton-promo2",
    storageBucket: "projet-louis-vuitton-promo2.appspot.com",
    messagingSenderId: "591929521600",

};

class Firebase {
    constructor() {
        app.initializeApp(config);

        this.auth = app.auth();

    }

    // *** Auth API ***


    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);
}

export default Firebase;
