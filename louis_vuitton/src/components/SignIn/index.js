import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {compose} from 'recompose';

import { PasswordForgetLink } from '../PasswordForget';
import './signIn.css';

import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ButtonToolbar, Form, Container, Row, Col,Button} from 'react-bootstrap';



const SignInPage = () => (
    <div>
        <div><h1 className="title1" align="center">1 0 0 1 M A L L E S</h1></div>

        <h2 className="title">C O N N E X I O N</h2>
        <SignInForm/>

    </div>
);
const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class SignInFormBase extends Component {
    constructor(props) {
        super(props);
        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {email, password} = this.state;
        this.props.firebase
            .doSignInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({...INITIAL_STATE});
                this.props.history.push(ROUTES.HOME);
            })
            .catch(error => {
                this.setState({error});
            });
        event.preventDefault();
    };
    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const {email, password, error} = this.state;
        const isInvalid = password === '' || email === '';
        return (
            <Container>
                <Row>
                    <Col className="m-auto col-md-3">
            <form onSubmit={this.onSubmit}>
                <Form.Group><Form.Control
                    name="email"
                    value={email}
                    onChange={this.onChange}
                    type="text"
                    placeholder="Identifiant ..."
                /></Form.Group>
                <Form.Group><Form.Control
                    name="password"
                    value={password}
                    onChange={this.onChange}
                    type="password"
                    placeholder="Mot de passe ..."
                /></Form.Group>
                <PasswordForgetLink />
                <ButtonToolbar>
                <Button className="mt-4" variant="secondary" size="lg" disabled={isInvalid} type="submit">
                    C O N N E X I O N
                </Button>
                </ButtonToolbar>
                <ButtonToolbar>
                    <Button className="mt-4" variant="dark" color= "white" size="lg">
                        <Link to={ROUTES.CREATIONCOMPTE}>DEMANDE DE CREATION DE COMPTE</Link>
                    </Button>
                </ButtonToolbar>
                <Form.Group>{error && <p>{error.message}</p>}</Form.Group>
            </form>
                </Col>
                </Row>
                </Container>
        );
    }
}

const SignInForm = compose(
    withRouter,
    withFirebase,
)(SignInFormBase);
export default SignInPage;
export {SignInForm};