import React, {Component} from 'react';
import {withFirebase} from '../Firebase';


import {Container, Row, Col, Form, ButtonToolbar, FormControl, Button} from "react-bootstrap";


const INITIAL_STATE = {
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

class PasswordChangeForm extends Component {
    constructor(props) {
        super(props);
        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {passwordOne} = this.state;
        this.props.firebase
            .doPasswordUpdate(passwordOne)
            .then(() => {
                this.setState({...INITIAL_STATE});
            })
            .catch(error => {
                this.setState({error});
            });
        event.preventDefault();
    };
    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const {passwordOne, passwordTwo, error} = this.state;
        const isInvalid =
            passwordOne !== passwordTwo || passwordOne === '';
        return (
            <Container>
                <Row>
                    <Col className="m-auto col-md-6">
                        <form onSubmit={this.onSubmit}>

                                <Form.Group>
                                <FormControl
                                    name="passwordOne"
                                    value={passwordOne}
                                    onChange={this.onChange}
                                    type="password"
                                    placeholder="Nouveau Mot de Passe"
                                />
                                </Form.Group>

                            <Form.Group>
                                <FormControl
                                    name="passwordTwo"
                                    value={passwordTwo}
                                    onChange={this.onChange}
                                    type="password"
                                    placeholder="Confirmer le Mot de Passe"
                                />
                            </Form.Group>

                                <ButtonToolbar>
                                    <Button className="mr-1 mt-4" variant="dark" size="lg" disabled={isInvalid} type="submit">
                                        Réinitialiser
                                    </Button>
                                </ButtonToolbar>
                                {error && <p>{error.message}</p>}
                        </form>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default withFirebase(PasswordChangeForm);