import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';
import {ButtonToolbar, Button, Form, Container, Row, Col} from "react-bootstrap";
import './passwordForget.css';
import 'bootstrap/dist/css/bootstrap.min.css';


const PasswordForgetPage = () => (
    <div>
        <h1 className="title">Mot de passe oublié?</h1>
        <PasswordForgetForm/>
    </div>
);
const INITIAL_STATE = {
    email: '',
    error: null,
};

class PasswordForgetFormBase extends Component {
    constructor(props) {
        super(props);
        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {email} = this.state;
        this.props.firebase
            .doPasswordReset(email)
            .then(() => {
                this.setState({...INITIAL_STATE});
            })
            .catch(error => {
                this.setState({error});
            });
        event.preventDefault();
    };
    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const {email, error} = this.state;
        const isInvalid = email === '';
        return (
            <Container>
                <Row>
                    <Col className="m-auto col-md-6">
                        <form onSubmit={this.onSubmit}>
                            <Form.Group>
                                <Form.Control
                                    name="email"
                                    value={this.state.email}
                                    onChange={this.onChange}
                                    type="text"
                                    className="font-italic"
                                    placeholder="Indiquer votre adresse mail..."
                                />
                            </Form.Group>

                            <ButtonToolbar>
                                <Button className="mr-1 mt-4" variant="secondary" type="button" size="lg">
                                    <Link to={ROUTES.SIGN_IN}>ANNULER</Link>
                                </Button>
                                <Button className="mr-1 mt-4" variant="dark" size="lg" disabled={isInvalid}
                                        type="submit">
                                    Envoyer
                                </Button>
                            </ButtonToolbar>

                            {error && <p>{error.message}</p>}
                        </form>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const PasswordForgetLink = () => (
    <ButtonToolbar>
    <p>
        <Link to={ROUTES.PASSWORD_FORGET}><div className="mot-de-passe">Mot de passe oublié?</div></Link>
    </p>
    </ButtonToolbar>
);
export default PasswordForgetPage;
const PasswordForgetForm = withFirebase(PasswordForgetFormBase);
export {PasswordForgetForm, PasswordForgetLink};