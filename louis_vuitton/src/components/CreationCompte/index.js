import React, {Component} from 'react';
import {Button, Form, Row, Col, ButtonToolbar, Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'firebase/app';
import {Link} from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

import axios from 'axios';


class Index extends React.Component {
    state = {
        lastName: '',
        firstName: '',
        mail: '',
        phone: '',
        mobile: '',
        department: '',
        address: '',
        zipCode: '',
        city: '',
        country: '',
    };

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    handleSubmit = event => {
        event.preventDefault();
        const user = {
            lastName: this.state.lastName,
            firstName: this.state.firstName,
            mail: this.state.mail,
            phone: this.state.phone,
            mobile: this.state.mobile,
            department: this.state.department,
            address: this.state.address,
            zipCode: this.state.zipCode,
            city: this.state.city,
            country: this.state.country,
        };
        this.props.history.push(ROUTES.SIGN_UP);

        axios.post(`https://us-central1-projet-louis-vuitton-promo2.cloudfunctions.net/doCreateUser
`, {user})
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    };

    render() {
        const {
            lastName,
            firstName,
            mail,
            phone,
            mobile,
            department,
            address,
            zipCode,
            city,
            country,
        } = this.state;

        const isInvalid =
            lastName === '' ||
            firstName === '' ||
            mail === '' ||
            phone === '' ||
            mobile === '' ||
            department === '' ||
            address === '' ||
            zipCode === '' ||
            city === '' ||
            country === '';

        return (
            <Container>
                <Row>
                    <Col className="m-auto col-md-6">
                        <h1 className="title">Demande de création de compte</h1>
                        <form onSubmit={this.handleSubmit}>
                            <Form.Group>
                                <Form.Control type="text" name="lastName"
                                              onChange={this.handleChange} placeholder="*Nom..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="firstName"
                                              onChange={this.handleChange} placeholder="*Prénom..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="mail"
                                              onChange={this.handleChange} placeholder="*E-mail..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="phone"
                                              onChange={this.handleChange} placeholder="*Telephone..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="mobile"
                                              onChange={this.handleChange} placeholder="*Mobile..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="department"
                                              onChange={this.handleChange} placeholder="*Department..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="address"
                                              onChange={this.handleChange} placeholder="*Adress..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="zipCode"
                                              onChange={this.handleChange} placeholder="*Code Postal..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="city"
                                              onChange={this.handleChange} placeholder="*Ville..."/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="country"
                                              onChange={this.handleChange} placeholder="*Pays..."/>
                            </Form.Group>
                            <ButtonToolbar>
                                <Button className="mr-3 mt-2" variant="dark" size="lg" type="button">
                                    <Link to={ROUTES.SIGN_IN}>Annuler</Link>

                                </Button>
                                <Button className="mr-3 mt-2" variant="secondary" size="lg"
                                        type="submit" disabled={isInvalid} >Envoyer
                                </Button>
                            </ButtonToolbar>
                        </form>
                    </Col>
                </Row>
            </Container>
        )
    }

}

export default Index;



