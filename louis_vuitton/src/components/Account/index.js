import React, {Component} from 'react';
import PasswordChangeForm from '../PasswordChange';

const AccountPage = () => (
    <div>
        <h1 className="title">Mon Compte</h1>

        <PasswordChangeForm />
    </div>
);
export default AccountPage;