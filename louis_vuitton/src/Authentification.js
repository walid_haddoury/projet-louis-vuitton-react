import React from 'react';
import {Button, Form, Row, Col, ButtonToolbar, Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import'./Authentification.css';

class Authentification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            motDePasse: ''
        };
    }
    mySubmitHandler = (event) => {
        event.preventDefault();
        console.log("You are submitting " + JSON.stringify(this.state));
        // convert to JSON format
    };
    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    };

    render() {
        return (
            <div>
            <Container>
                <Row>
                    <Col className="m-auto col-md-4" >
                        <p className="connexion">C O N N E X I O N</p>
                        <Form ref={form => this.form = form} >
                            <Form.Group>
                                <Form.Control type="text" name="email" value={this.state.lastname}
                                              onChange={this.myChangeHandler} placeholder="jean@mot-tiff.com" />
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="text" name="mot de passe" value={this.state.firstname}
                                              onChange={this.myChangeHandler} placeholder="**********" />
                            </Form.Group>
                            <p className="motdepasse">Mot de passe oublié ?</p>
                            <ButtonToolbar>
                                <Button className="mt-4" variant="secondary" size="lg" type="button" onClick={this.handleCancelClick}>
                                    CONNEXION
                                </Button>
                                <br/>
                                <Button className="mt-4" variant="dark" size="lg" type="button" onClick={this.mySubmitHandler}>
                                    DEMANDE DE CREATION DE COMPTE
                                </Button>

                            </ButtonToolbar>

                        </Form>
                    </Col>
                </Row>
            </Container>
            </div>
        );
    }
}
export default Authentification;