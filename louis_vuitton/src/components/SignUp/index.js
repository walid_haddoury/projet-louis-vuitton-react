import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {compose} from 'recompose';

import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';

import {Form, Container, Row, Col, ButtonToolbar, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const SignUpPage = () => (
    <div>
        <h1 className="title">CREATION DE COMPTE</h1>
        <SignUpForm/>
    </div>
);


const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

class SignUpFormBase extends Component {
    constructor(props) {
        super(props);

        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {username, email, passwordOne} = this.state;

        this.props.firebase
            .doCreateUserWithEmailAndPassword(email, passwordOne)
            .then(authUser => {
                this.setState({...INITIAL_STATE});
                this.props.history.push(ROUTES.HOME);

            })
            .catch(error => {
                this.setState({error});
            });
        event.preventDefault();
    }
    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };


    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
        } = this.state;


        const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '' ||
            email === '' ||
            username === '';

        return (
            <Container>
                <Row>
                    <Col>
                        <form onSubmit={this.onSubmit}>
                            <Form.Group>
                                <Form.Control
                                    name="username"
                                    value={username}
                                    onChange={this.onChange}
                                    type="text"
                                    placeholder="Full Name"
                                /></Form.Group>
                            <Form.Group><Form.Control
                                name="email"
                                value={email}
                                onChange={this.onChange}
                                type="text"
                                placeholder="Email Address"
                            /></Form.Group>
                            <Form.Group> <Form.Control
                                name="passwordOne"
                                value={passwordOne}
                                onChange={this.onChange}
                                type="password"
                                placeholder="Password"
                            /></Form.Group>
                            <Form.Group><Form.Control
                                name="passwordTwo"
                                value={passwordTwo}
                                onChange={this.onChange}
                                type="password"
                                placeholder="Confirm Password"
                            /></Form.Group>
                            <ButtonToolbar><Button className="mr-3 mt-4" variant="secondary" disabled={isInvalid}  type="submit">
                                VALIDER
                            </Button>
                                <Button className="mr-3 mt-4" variant="dark" size="lg" type="submit">
                                    <Link to={ROUTES.CREATIONCOMPTE}><div className="creation">ANNULER</div></Link>
                                </Button>
                            </ButtonToolbar>
                            {error && <p>{error.message}</p>}
                        </form>
                    </Col>
                </Row>
            </Container>
        );
    }
}


/*const SignUpLink = () => (
    <Form.Group>
        <ButtonToolbar>
        <Button className="creation" variant="dark" color= "light" size="lg">
            <Link to={ROUTES.CREATIONCOMPTE}>DEMANDE DE CREATION DE COMPTE</Link>
        </Button>
        </ButtonToolbar>
    </Form.Group>

);*/


const SignUpForm = compose(
    withRouter,
    withFirebase,
)(SignUpFormBase);


export default SignUpPage;

export {SignUpForm};