import React from 'react';
import {Nav} from 'react-bootstrap';
import { FaHome , FaSuitcase, FaUserAlt, FaMapMarker, FaArchive, FaObjectGroup, FaPrint, FaBell, FaShoppingCart, FaStreetView } from 'react-icons/fa';
import './sidebar.css';
import * as ROUTES from '../../constants/routes';


const Sidebar = () => {
    return (
        <div className="App">
            <Nav defaultActiveKey="/" className="flex-column">
                <h1 className="logo"> <strong>1001</strong> <br/><span>MALLES</span> </h1>
                <Nav.Link href={ROUTES.HOME}><FaHome className ="icons"/> Home</Nav.Link>
                <Nav.Link href="/Collections"><FaSuitcase className ="icons"/> Collections</Nav.Link>
                <Nav.Link href="/Proprietaires"><FaUserAlt className ="icons"/>Propriétaires</Nav.Link>
                <Nav.Link href="/Emplacements"><FaMapMarker className ="icons"/>Emplacements</Nav.Link>
                <Nav.Link href="/Applications"><FaArchive className ="icons"/>Applications</Nav.Link>
                <Nav.Link href="/Contenu Génériques"><FaObjectGroup className ="icons"/>Contenu Génériques</Nav.Link>
                <hr/>
                <Nav.Link href="/Tickets"><FaPrint className ="icons"/>Tickets</Nav.Link>
                <Nav.Link href="/Notifications"><FaBell className ="icons"/>Notifications</Nav.Link>
                <Nav.Link href="/Panier"><FaShoppingCart className ="icons"/>Panier</Nav.Link>
                <Nav.Link href="/Profils"><FaStreetView className ="icons"/>Profils</Nav.Link>
            </Nav>
        </div>
    );
};
export default Sidebar;